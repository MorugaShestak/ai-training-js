const brain = require('brain.js');

const config = {
    binaryThresh: 0.5,
    hiddenLayers: [2],
    activation: 'sigmoid',
};

const net = new brain.NeuralNetwork(config);

net.train(
    [
        {input: [0, 0], output: [0]},
        {input: [0, 1], output: [1]},
        {input: [1, 0], output: [1]},
        {input: [1, 1], output: [0]},
    ]
);

let results = [];

for (let i = 0; i < 10000; i++) {
    let a = Math.round(Math.random());
    let b = Math.round(Math.random());
    results.push([[a, b], net.run([a, b])]);
}

console.log(results);
